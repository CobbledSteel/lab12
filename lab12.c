/******************************************************************************
* Assignment:  lab12
* Lab Section: SC189 15:30 SC189 
* Description: Compare and find the preferred input set by checking that the 
*              numbers in the smaller set are all included in the bigger set. 
*              If the arrays are the same, display that as the result, and if
*              they are not compatible, print that they are so.
* Programmers: Vadim Nikiforov vnikifor@purdue.edu
*              Ziheng wang wang2194@purdue.edu
*              Haozheng Qu qu34@purdue.edu
******************************************************************************/
#include <stdio.h>
#include <math.h>
#define SIZE 20

// function declarations
int getArray(int[], int);
void sortArray(int[], int);
int compareArrays(int[], int[], int, int);
void printAnswer(int, int, int);

int main(void)
{
  // local declarations
  int array1[SIZE]; // the first array the user enters
  int size1; // the size of the first array the user enters
  int array2[SIZE]; // the second array the user enters
  int size2; // the size of the second array the user enters
  int answer; // a variable that represents the comparison of the two sets

  // obtain user data
  size1 = getArray(array1, 1);
  sortArray(array1, size1);
  size2 = getArray(array2, 1);
  sortArray(array2, size2);

  answer = compareArrays(array1, array2, size1, size2); // compare the arrays

  printAnswer(answer, size1, size2); // print the final results
  
  return 0;
}

/******************************************************************************
* Function:    printAnswer
* Description: print the answer to the problem; whether or not the sets are
*              the same, not comparable, or if one is preferred
* Parameters:  comparison, int, whether or not the sets are the same
*              firstSize, int, the size of the first array
*              secondSize, int, the size of the second array
* Return:      void
******************************************************************************/
void printAnswer(int comparison, int firstSize, int secondSize)
{
  printf("\n");// add a new line
  if(comparison && firstSize == secondSize) // say if they are the same
    printf("The two data sets are the same.\n");
  else if(comparison && firstSize > secondSize) // the bigger sets are preferred
    printf("The first set is the preferred set.\n");
  else if(comparison && firstSize < secondSize)
    printf("The second set is the preferred set.\n");
  else // say if they are not compatible
    printf("The two data sets are not comparable.\n");
}

/******************************************************************************
* Function:    getArray
* Description: obtains an array from the user, displaying a number representing
*              how often the user has been asked
* Parameters:  inputArray, int[], the array to store
*              timesAsked, int, the number of times the user has been prompted
* Return:      int, the number of meaningful values obtained
******************************************************************************/
int getArray(int inputArray[], int timesAsked)
{
  // local declarations
  int i; // loop control variable
  int doneYet; // whether or not the user is done adding values
  int size; // the number of meaningful data points

  printf("Enter Data Set #%d: ", timesAsked);

  i = 0;
  doneYet = 0;
  // get values until the maximum size or -1
  while(i < SIZE && !doneYet)
  {
      scanf("%d", &inputArray[i]);
      doneYet = inputArray[i] == -1;
      i += inputArray[i] != -1;
  }
  size = i;
  return size;
}

/******************************************************************************
* Function:    sortArray
* Description: sorts the array using selection sort, based on an algorithm that
*              was discussed in lectures
* Parameters:  unsorted, int[], the unsorted array to change
*              size, int, the number of meaningful data points in the array
* Return:      void
******************************************************************************/
void sortArray(int unsorted[], int size)
{

  // local declarations
  int i; // loop control variable
  int j; // loop control variable
  int min; // the minimum value in a pass
  int temp; // a temporary value for swapping

  // increment the end of the sorted values
  for(i = 0; i < size; i++)
  {
    min = i;
    // search through the unsorted values
    for(j = i + 1; j <size; j++)
    {
      if(unsorted[j] < unsorted[min])
        min = j;
    }
    // swap the next minimum into the proper place
    if (i != min)
    {
      temp = unsorted[i];
      unsorted[i] = unsorted[min];
      unsorted[min] = temp;
    }
  }
  return;
}

/******************************************************************************
* Function:    compareArrays
* Description: compares two arrays, returning whether they are compatible
* Parameters:  first, int[], the first array to compare
*              second, int[], the second array to compare
*              firstSize, int, the number of data points in the first array
*              secondSize, int, the number of data points in the second array
* Return:      int, whether or not the data sets are compatible
******************************************************************************/
int compareArrays(int first[], int second[], int firstSize, int secondSize)
{
  // local declarations
  int results; // whether or not the data sets are compatible
  int i; // loop control variable
  int j; // loop control variable

  i = 0;
  j = 0;
  results = 1;
  // continue until a result is found or the bounds of the array are exceeded
  while(results && i < firstSize && j < secondSize)
  {
    if(firstSize > secondSize)
    {
      // if two compared values are the same, increment both arrays
      if(first[i] == second[j])
      {
        i++;
        j++;
      }
      // if the bigger array contains an extra element, increment the bigger array
      else if(first[i] < second[j])
      {
        i++;
        // return incompatible if the array bounds are exceeded
        if(i >= firstSize)
          results = 0;
      }
      // if the smaller contains an extra element, they are not compatible
      else if(first[i] > second[j])
        results = 0;
    }
    else if(secondSize > firstSize)
    {
      // if two compared values are the same, increment both arrays
      if(second[j] == first[i])
      {
        i++;
        j++;
      }
      // if the bigger array contains an extra element, increment the bigger array
      else if(second[j] < first[i])
      {
        j++;
        // return incompatible if the array bounds are exceeded
        if(j >= secondSize)
          results = 0;
      }
      // if the smaller contains an extra element, they are not compatible
      else if(second[j] > first[i])
        results = 0;
    }
    else
    {
      // if the arrays are the same size, they must have the exact same content to be compatible
      if(first[i] == second[j])
      {
        j++;
        i++;
      }
      else
        results = 0;
    }
  }

  return results;
}
